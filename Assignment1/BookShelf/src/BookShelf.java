
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.*;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.xml.sax.SAXException;

public class BookShelf {

	private String xmlFilename;
	private final String XML_SCHEMA_NAME = "schema.xsd";

	public BookShelf(String xmlFilename) {
		this.xmlFilename = xmlFilename;
	}	

	public static void main(String[] args) {
		BookShelf shelf = new BookShelf(args[0]);
		if (args[1].equals("list"))
		{
			if (shelf.validate(true))
			{
				shelf.list();
			}
		}
		else if (args[1].equals("validate"))
		{
			shelf.validate(false);
		}
		
		else if (args[1].equals("xpath"))
		{
			shelf.xpath(args[2]);
		}		
	}

	//Returns true if the document is valid.
	public boolean validate(boolean silentSuccess) {
		// source: http://stackoverflow.com/questions/13884084/java-xml-schema-validation
        Source xmlFile = new StreamSource(new File(xmlFilename));
		Source schemaFile = new StreamSource(new File(XML_SCHEMA_NAME));

        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema;
        try{
        	schema = schemaFactory.newSchema(schemaFile);
        	Validator validator = schema.newValidator();
            validator.validate(xmlFile);
            if (!silentSuccess)
            {
                System.out.println(xmlFile.getSystemId() + " is valid");
            }
            return true;
        }
        catch (SAXException e) 
        {
            System.out.println(xmlFile.getSystemId() + " is NOT valid");
            System.out.println("Reason: " + e.getLocalizedMessage());
        }
        catch (IOException e) {
        	System.out.println(e.getLocalizedMessage());
		}
        return false;
	}

	public void list() {
		// http://www.mkyong.com/java/how-to-read-xml-file-in-java-dom-parser/

		try {
			File xmlFile = new File(xmlFilename);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder;
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);
			
			doc.getDocumentElement().normalize();
		 							
			printBookShelves(doc.getElementsByTagName("bookshelf"));			
			printUnknownDump(doc.getElementsByTagName("unknown").item(0));
			printBookSeries(doc.getElementsByTagName("seriesItem"));
			
		} catch (ParserConfigurationException e) {
        	System.out.println(e.getLocalizedMessage());
		} catch (SAXException e) {
        	System.out.println(e.getLocalizedMessage());
		} catch (IOException e) {
        	System.out.println("Error xml file: " + xmlFilename);
		}
	}
	
	
	
	private void printBookShelves(NodeList shelves) {
		for (int i = 0; i < shelves.getLength(); i++) 
		{
			printBookshelf(shelves.item(i));
		}
	}

	private void printBookshelf(Node shelfNode)
	{
		int width, cumulativeWidth = 0;
		NamedNodeMap attributes = shelfNode.getAttributes();
		System.out.println("*****NEW BOOKSHELF*****");
		System.out.println("Shelf ID: " + attributes.getNamedItem("id").getNodeValue());
		Element shelfElement = (Element) shelfNode;
		width = Integer.parseInt(shelfElement.getElementsByTagName("width").item(0).getTextContent());
		System.out.println("Shelf width: " + String.valueOf(width));
		
		System.out.println("Shelf books:");
		NodeList books = shelfElement.getElementsByTagName("book");
		for (int i = 0; i < books.getLength(); i++) 
		{
			cumulativeWidth += printBook((Element) books.item(i));
		}
		System.out.println("---End of books---");
		System.out.println("Shelf space left: " + String.valueOf(width - cumulativeWidth));
		
	}
	
	private void printUnknownDump(Node unknownNode)
	{
		System.out.println("*****BOOKS IN UNKNOWN LOCATION*****");
		NodeList books = ((Element)unknownNode).getElementsByTagName("book");
		for (int i = 0; i < books.getLength(); i++) 
		{
			printBook((Element) books.item(i));
		}
		System.out.println("---End of books---");
	}
		
	//returns the thickness of the book.
	private int printBook(Element book)
	{
		System.out.println("---New book---");
		int bookWidth = Integer.parseInt(book.getElementsByTagName("thickness").item(0).getTextContent());
		NamedNodeMap attributes = book.getAttributes();
		
		System.out.println("Book ID: " + attributes.getNamedItem("id").getNodeValue());
		printElement("Title", book.getElementsByTagName("name"));
		printAuthors(book.getElementsByTagName("author"));
		printElement("Thickness", book.getElementsByTagName("thickness"));
		printElement("Language", book.getElementsByTagName("language"));
		printAdditionalMaterials(book.getElementsByTagName("additionalMaterials"));
		printElement("CoverMaterial", book.getElementsByTagName("coverMaterial"));
		printElement("Condition", book.getElementsByTagName("condition"));
		printElement("Enjoyability", book.getElementsByTagName("enjoyability"));
		printElement("Genre", book.getElementsByTagName("genre"));
		printElement("Series ID", book.getElementsByTagName("series"));
		printElement("Owner", book.getElementsByTagName("owner"));
		
		return bookWidth;
	}
	
	private void printElement(String elementName, NodeList node)
	{
		if (node.getLength() > 0)
		{
			printNameAndContent(elementName, node.item(0).getTextContent());	
		}
	}
	
	
	
	private void printNameAndContent(String elementName, String textContent) {
		System.out.println(elementName +": " + textContent);		
	}

	private void printAuthors(NodeList authors)
	{
		StringBuilder builder = new StringBuilder();
		if (authors.getLength() > 1) 
		{
			builder.append("Authors: ");
		}
		else 
		{
			builder.append("Author: ");
		}
		for (int i = 0; i < authors.getLength(); i++)
		{
			if (i != 0)
			{
				builder.append(", ");
			}
			builder.append(authors.item(i).getTextContent());
		}
		System.out.println(builder.toString());
	}
	
	private void printAdditionalMaterials(NodeList materialsList)
	{
		if (materialsList.getLength() == 0) 
		{
			return;
		}
		NodeList materials = materialsList.item(0).getChildNodes();
		boolean first = true;
		StringBuilder builder = new StringBuilder();
		builder.append("Additional Materials: ");
		for (int i = 0; i < materials.getLength(); i++)
		{
			if (materials.item(i).getNodeName().equals("#text"))
			{
				continue;
			}
			if (!first)
			{
				builder.append(", ");
			}
			builder.append("Material type: ");
			builder.append(materials.item(i).getNodeName());
			builder.append(", Material name: ");
			builder.append(materials.item(i).getTextContent());
			first = false;
		}
		System.out.println(builder.toString());
	}
	
	private void printBookSeries(NodeList series)
	{
		if (series.getLength() > 0)
		{
			System.out.println("***BOOKSERIES***");
		}
		for (int i = 0; i < series.getLength(); i++)
		{
			System.out.println("---new series---");

			
			NamedNodeMap attributes = series.item(i).getAttributes();
			System.out.println("Series ID: " + attributes.getNamedItem("id").getNodeValue());
			
			NodeList books = series.item(i).getChildNodes();
			int volume = 1;
			for (int j = 0; j < books.getLength(); j++)
			{
				if (books.item(j).getNodeName().equals("bookId"))
				{
					System.out.println(String.valueOf(volume) + ": Book ID: " + books.item(j).getTextContent());
					volume++;
				}
			}
			
			System.out.println("---End of bookseries---");
		}
	}
	
	public void xpath(String xpath) {
		try {
			NodeList results = GetXPathResults(xpath);            
            
            if (results.getLength() == 0)
            	System.out.println("No results.");
            
            printXPathResults(results);
            
		} catch (Exception e) {
			System.err.println("Error in XPath search!");
			e.printStackTrace();
		}
	}

	private void printXPathResults(NodeList results) {
		for (int i = 0; i < results.getLength(); i++) {                
        	Node node = results.item(i);            	            	
        	String name = node.getNodeName(); 
        	
        	if (name.equals("book"))
        		printBook((Element) node);
        	
        	else if (name.equals("bookshelf"))
        		printBookshelf(node);
        	
        	else if (name.equals("unknown"))
        		printUnknownDump(node);
        	
        	else          		        		
    			printNameAndContent(name, node.getTextContent());        		        		
        }		
	}

	private NodeList GetXPathResults(String xpath) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {
		FileInputStream file = new FileInputStream(new File(xmlFilename));                 
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();             
        DocumentBuilder builder =  builderFactory.newDocumentBuilder();             
        Document xmlDocument = builder.parse(file);

        XPath xPathObject =  XPathFactory.newInstance().newXPath();           
        System.out.println("Results for the following XPath expression: " + xpath);
                    
        return (NodeList) xPathObject.compile(xpath).evaluate(xmlDocument, XPathConstants.NODESET);
	}
}
