package com.example.helloworlder;

import java.io.InputStream;
import java.net.URL;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * 
 */
public class DisplayImage extends Fragment implements OnClickListener {

	ImageView imageContainer;
	TextView errorMessage;
	
	public DisplayImage() {
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		// Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_display_image, container, false);
        
        imageContainer = (ImageView) v.findViewById(R.id.image_container);

    	errorMessage = (TextView) v.findViewById(R.id.error_msg);
    	
        Button b = (Button) v.findViewById(R.id.image_fetch_button);
        b.setOnClickListener(this);
        
		return v;
	}

	public void fetchImage(View view)
	{	
		EditText urlString = (EditText) getView().findViewById(R.id.image_url);
		errorMessage.setText("");
    	ImageLoader loader = new ImageLoader(getActivity());
    	loader.execute(urlString.getText().toString());
	}

	//from: http://stackoverflow.com/questions/6091194/how-to-handle-button-clicks-using-the-xml-onclick-within-fragments
	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		
		case(R.id.image_fetch_button):
			fetchImage(view);
			break;
		}
		
	}
	
	private class ImageLoader extends AsyncTask<String, Void, Bitmap>
	{
		Activity activity;

		public ImageLoader(Activity _act)
		{
			activity = _act;
		}
		@Override
		protected Bitmap doInBackground(String... params) {
			try{

				//from: http://stackoverflow.com/questions/14867278/android-app-display-image-from-url
		        URL url = new URL(params[0]);
		        HttpGet httpRequest = null;
		        httpRequest = new HttpGet(url.toURI());
		        HttpClient httpclient = new DefaultHttpClient();
		        HttpResponse response = (HttpResponse) httpclient.execute(httpRequest);
	
		        HttpEntity entity = response.getEntity();
		        BufferedHttpEntity b_entity = new BufferedHttpEntity(entity);
		        InputStream input = b_entity.getContent();
	
		        return BitmapFactory.decodeStream(input);	
			}
			catch(Exception ex)
			{
		    	activity.runOnUiThread(new Runnable() {
					@Override
					public void run() {   
						errorMessage.setText(getString(R.string.error_textbox));
					}
	            });
				//ex.printStackTrace();
				return null;
			}
		}
		
		@Override
	    protected void onPostExecute(final Bitmap bitmap) {
	    	activity.runOnUiThread(new Runnable() {
				@Override
				public void run() {   
			    	imageContainer.setImageBitmap(bitmap);
				}
            });
	    }
	}
}
