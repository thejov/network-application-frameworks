package com.example.helloworlder;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;

import javax.net.ssl.HttpsURLConnection;


/**
 * A simple {@link android.support.v4.app.Fragment} subclass. Activities that
 * contain this fragment must implement the
 * {@link HelloTextTab.OnFragmentInteractionListener} interface to handle interaction
 * events. Use the {@link HelloTextTab#newInstance} factory method to create an
 * instance of this fragment.
 * 
 */
public class HelloTextTab extends Fragment {
	
	public HelloTextTab() {
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		new RetrieveTextFileTask(getActivity()).execute();
		
		LayoutInflater lf = getActivity().getLayoutInflater();
        View view = lf.inflate(R.layout.fragment_hello_text_tab, container, false);
		return view;
	}

}

class RetrieveTextFileTask extends AsyncTask<Void, Void, String> {

	Activity activity;
	
	public RetrieveTextFileTask(Activity _activity)
	{
		activity = _activity;
	}
	@Override
	protected String doInBackground(Void... params) {
		String urlString = "https://playground.cs.hut.fi/t-110.5140/hello.txt";
		String result = "";
		
		try {
			URL url = new URL(urlString);
			HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
			
			BufferedReader in = new BufferedReader(
					new InputStreamReader(connection.getInputStream()));			
			
			String line;			
			while ((line = in.readLine()) != null)
			    result += line;

			result += "\nLast modified: " + new Date(connection.getLastModified());
			in.close();					

		} catch (Exception e) {			
			e.printStackTrace();
			result = "Retrieving file failed.";
		}
		return result;	
	}
	
	protected void onPostExecute(final String result) {
		activity.runOnUiThread(new Runnable() {
			@Override
			public void run()
			{
				TextView textView = (TextView) activity.findViewById(R.id.helloTextBox);
		        textView.setText(result);
			}
		});		
    }
}
