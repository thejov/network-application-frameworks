package com.example.helloworlder;

import java.net.URL;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class WeatherTab extends Fragment implements OnClickListener {

	TextView weatherContainer;
	TextView errorMessage;

    String rssResult = "";
    boolean item = false;
    
	public WeatherTab() {
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_weather_tab, container, false);

        weatherContainer = (TextView) v.findViewById(R.id.weather_container);

        weatherContainer.setMovementMethod(new ScrollingMovementMethod());
        
    	errorMessage = (TextView) v.findViewById(R.id.weather_error_msg);
    	
        Button b = (Button) v.findViewById(R.id.weather_fetch_button);
        b.setOnClickListener(this);
        
		return v;
	}

	//from: http://stackoverflow.com/questions/6091194/how-to-handle-button-clicks-using-the-xml-onclick-within-fragments
	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		
		case(R.id.weather_fetch_button):
			fetchWeather(view);
			break;
		}
		
	}
	
	private void fetchWeather(View view)
	{
		EditText urlString = (EditText) getView().findViewById(R.id.weather_woeid);
		errorMessage.setText("");
		WeatherLoader loader = new WeatherLoader(getActivity());
    	loader.execute("http://weather.yahooapis.com/forecastrss?w=" + urlString.getText().toString() + "&u=c");
	}
	
	public class WeatherLoader extends AsyncTask<String, Void, Void>
	{
		Activity activity;

		public WeatherLoader(Activity _act)
		{
			activity = _act;
		}
		@Override
		protected Void doInBackground(String... params) {
	        try {
	            URL rssUrl = new URL(params[0]);
	            SAXParserFactory factory = SAXParserFactory.newInstance();
	            SAXParser saxParser = factory.newSAXParser();
	            XMLReader xmlReader = saxParser.getXMLReader();
	            RSSHandler rssHandler = new RSSHandler();
	            xmlReader.setContentHandler(rssHandler);
	            InputSource inputSource = new InputSource(rssUrl.openStream());
	            xmlReader.parse(inputSource);

	        } catch (Exception e) {
	        	e.printStackTrace();
	        }
	        return null;
		}
		
		@Override
	    protected void onPostExecute(final Void result) {
			
	    	activity.runOnUiThread(new Runnable() {
				@Override
				public void run() {   
			    	weatherContainer.setText(Html.fromHtml(rssResult));
				}
            });
	    }
	}

	//From: http://www.javaworld.com/article/2078529/java-android-developer/java-tip--set-up-an-rss-feed-for-your-android-application.html
	private class RSSHandler extends DefaultHandler {

        public void startElement(String uri, String localName, String qName,
                Attributes attrs) throws SAXException {
            if (localName.equals("item") || localName.equals("pubDate") || localName.equals("lat") || localName.equals("long") || localName.equals("description"))
            {
                item = true;
            }
            else
            {
            	item = false;
            }

            if (!localName.equals("item") && item == true)
                rssResult = rssResult + localName + ": ";

        }

        public void endElement(String namespaceURI, String localName, String qName) throws SAXException {

        }

        public void characters(char[] ch, int start, int length)
                throws SAXException {
            String cdata = new String(ch, start, length);
            if (item == true)
                rssResult = rssResult +(cdata.trim()).replaceAll("\\s+", " ")+"\t";

        }

    }
}
