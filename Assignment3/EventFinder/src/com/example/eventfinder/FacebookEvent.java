package com.example.eventfinder;

import org.json.JSONException;
import org.json.JSONObject;

public class FacebookEvent {
	public String id;
	private String name;
	private String location;
	private String startTime;

	public FacebookEvent(JSONObject event) {
		try {
			id = (String) event.get("eid");
			name = (String) event.get("name");
			location = (String) event.get("location");
			startTime = (String) event.get("start_time");
		} catch (JSONException e) {
			e.printStackTrace();
		}		
	}
	
	public String toString() {
		return name + "\n" + location + "\n" + startTime;
	}
	
	public String getName() {
		return name;
	}
}


/*"name": "TissunTai", 
"location": "The Lane Viihdekeskus", 
"start_time": "2014-04-22T17:00:00+0300", 
"timezone": "Europe/Helsinki"*/