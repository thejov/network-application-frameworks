package com.example.eventfinder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.facebook.*;
import com.facebook.Request.GraphUserCallback;
import com.facebook.model.*;

public class MainActivity extends ActionBarActivity {

	double longitude = 0, latitude = 0;
	private ArrayList<FacebookEvent> facebookEvents = new ArrayList<FacebookEvent>();
	private boolean queryRan = false;
	private GraphUser user = null;
	private Activity mainAct = this;
	
	private static final List<String> readPermissions = Arrays.asList("user_friends", "user_events", "friends_events");
	private static final List<String> publishPermissions = Arrays.asList("rsvp_event");
	private LocationManager locationManager;
	private LocationListener locationListener;

	@Override
	public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);
		initFacebook();
		initGPS();
	}

	private void initGPS() {
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			locationListener = new MyLocationListener();
			locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10,
					locationListener);
		} else {
			Toast.makeText(this, "Please enable GPS!", Toast.LENGTH_SHORT).show();
		}

	}

	private void initFacebook() {
		Toast.makeText(getBaseContext(), "Logging in with Facebook...", Toast.LENGTH_SHORT).show();
		
		Session mFacebookSession = Session.getActiveSession();
		if (mFacebookSession == null || mFacebookSession.isClosed()) {
			mFacebookSession = new Session(this);
		}
		
		Session.setActiveSession(mFacebookSession);

		Session.StatusCallback mCallback = new Session.StatusCallback() {

			// callback when session changes state
			@Override
			public void call(Session session, SessionState state,
					Exception exception) {
				if (session.isOpened()) {
					// make request to the /me API
					Request.newMeRequest(session, getGraphUserCallback())
							.executeAsync();
				}
			}
		};
		Session.OpenRequest readRequest = new Session.OpenRequest(this);
		readRequest.setPermissions(readPermissions);
		readRequest.setCallback(mCallback);
		mFacebookSession.openForRead(readRequest);		
	}
	
	private GraphUserCallback getGraphUserCallback() {
		return new Request.GraphUserCallback() {
	
			// callback after Graph API response with user object
			@Override
			public void onCompleted(GraphUser _user, Response response) {
				user = _user;
				Session mFacebookSession = Session.getActiveSession();
				if (!isSubsetOf(publishPermissions, mFacebookSession.getPermissions())) {					 
					Session.NewPermissionsRequest publishPermissionRequest = new Session.NewPermissionsRequest(mainAct, publishPermissions);
					mFacebookSession.requestNewPublishPermissions(publishPermissionRequest);
				}
			}
		};
	}

	private boolean isSubsetOf(List<String> subset, List<String> superset) {
	    for (String string : subset) {
	        if (!superset.contains(string)) {
	            return false;
	        }
	    }
	    return true;
	}
	
	private void makeFQLRequest() {
		if (queryRan == false) {
			queryRan = true;
		} else {
			return;
		}
		
		Toast.makeText(getBaseContext(), "Retrieving Facebook events...", Toast.LENGTH_SHORT).show();
		
        Session session = Session.getActiveSession();
        
		String fqlQuery = "SELECT eid, name, location, start_time FROM event WHERE eid IN (SELECT eid " +
				"FROM event_member WHERE uid IN (SELECT uid1, uid2 FROM friend WHERE uid2 = me())) AND " + 
				"venue.longitude < '" +	( longitude + 0.1 ) + "' AND venue.latitude < '" + ( latitude + 0.1 ) + 
				"' AND venue.longitude > '" + ( longitude - 0.1 ) + "' AND venue.latitude > '" + ( latitude - 0.1 ) + 
				"' AND start_time > now() ORDER BY start_time ASC";
		Log.d("query", fqlQuery);
        Bundle params = new Bundle();
        params.putString("q", fqlQuery);
        Request HTTPrequest = new Request(session,
            "/fql",                         
            params,                         
            HttpMethod.GET,                 
            new Request.Callback(){                       
				public void onCompleted(Response response) {
                    parseJSONResponse(response);
                    if (!facebookEvents.isEmpty()) {
                    	listEventsInGUI();
                    	locationManager.removeUpdates(locationListener);
                    }
                }			                
        }); 
        Request.executeBatchAsync(HTTPrequest);
	}
	
	private void parseJSONResponse(Response response) {
		Log.d("Response", response.toString());
        try {
        	JSONObject jsonObj = response.getGraphObject().getInnerJSONObject();
        	JSONArray events = jsonObj.getJSONArray("data");
        	facebookEvents.clear();
			
        	for (int i = 0; i < events.length(); i++) {
			    FacebookEvent event = new FacebookEvent(events.getJSONObject(i));
			    facebookEvents.add(event);
			}
			Log.d("Parsed event list", facebookEvents.toString());
		} catch (JSONException e) {		
			e.printStackTrace();
		}		
	}  
	
	private void listEventsInGUI() {
		final ListView listview = (ListView) findViewById(R.id.listview);

		final StableArrayAdapter adapter = new StableArrayAdapter(this,
				android.R.layout.simple_list_item_1, facebookEvents);
		listview.setAdapter(adapter);

		listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
				final FacebookEvent facebookEvent = (FacebookEvent) parent.getItemAtPosition(position);
				generateJoinDialog(facebookEvent);							
			}

		});
	}
	

	private void generateJoinDialog(final FacebookEvent event) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Please confirm!");
		builder.setMessage ("Do you want to join event '" + event.getName() + "'");
		
		DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick (DialogInterface dialog, int which) {
				if (which == DialogInterface.BUTTON_POSITIVE) {
					joinFacebookEvent(event);
					Log.d("butan", "Y");
				}
				else if(which == DialogInterface.BUTTON_NEGATIVE) {
					Log.d("butan", "N");
				}
			}
		};
		builder.setPositiveButton("Yes", listener);
		builder.setNegativeButton("No", listener);
		builder.setCancelable(false);
		builder.show ();
	}

	private void joinFacebookEvent(FacebookEvent facebookEvent) {		
		new Request(
				Session.getActiveSession(),
			    "/" + facebookEvent.id + "/attending",
			    null,
			    HttpMethod.POST,
			    new Request.Callback() {
			        public void onCompleted(Response response) {
			            /* handle the result */
			        	Log.d("Join response", response.toString());
			        	if (joinSuccessful(response))
			        		Toast.makeText(getBaseContext(), "Successfully joined event.", Toast.LENGTH_SHORT).show();
			        }					
			    }
			).executeAsync();
	}
	
	private boolean joinSuccessful(Response response) {
		try {
			return Boolean.parseBoolean(response.getGraphObject().getInnerJSONObject().getString("FACEBOOK_NON_JSON_RESULT"));
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Session.getActiveSession().onActivityResult(this, requestCode,
				resultCode, data);
	}

	private class MyLocationListener implements LocationListener {

		@Override
		public void onLocationChanged(Location loc) {
				Toast.makeText(getBaseContext(), "Getting GPS location...", Toast.LENGTH_SHORT).show();
	    		
				latitude = loc.getLatitude();
				longitude = loc.getLongitude();
				Log.d("loc", "lat: " + latitude);
				if (loggedIn()) {
					makeFQLRequest();	
				}				    	
		}
		
		private boolean loggedIn() {
			return user != null;
		}

		@Override
		public void onProviderDisabled(String provider) {
		}

		@Override
		public void onProviderEnabled(String provider) {
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
	}

	//http://www.vogella.com/tutorials/AndroidListView/article.html#androidlists_overview
	private class StableArrayAdapter extends ArrayAdapter<FacebookEvent> {

		HashMap<FacebookEvent, Integer> mIdMap = new HashMap<FacebookEvent, Integer>();

		public StableArrayAdapter(Context context, int textViewResourceId,
				List<FacebookEvent> facebookEvents) {
			super(context, textViewResourceId, facebookEvents);
			for (int i = 0; i < facebookEvents.size(); ++i) {
				mIdMap.put(facebookEvents.get(i), i);
			}
		}

		@Override
		public long getItemId(int position) {
			FacebookEvent item = getItem(position);
			return mIdMap.get(item);
		}

		@Override
		public boolean hasStableIds() {
			return true;
		}

	}
}
