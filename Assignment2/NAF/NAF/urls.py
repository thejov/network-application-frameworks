from django.conf.urls import patterns, include, url
from Assignment2.views import *
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'NAF.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^map/', gmaps),
    url(r'^login/', fb_login),
    url(r'^canvas/', canvas),
)