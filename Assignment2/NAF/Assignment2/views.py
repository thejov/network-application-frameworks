from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.clickjacking import xframe_options_exempt

def gmaps(request):
    return render(request, 'gmaps.html')

def fb_login(request):
    return render(request, 'login.html')

@xframe_options_exempt
@csrf_exempt
def canvas(request):
    return render(request, 'canvas.html')