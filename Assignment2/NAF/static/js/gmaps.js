

function initialize() {
  var myLatlng = new google.maps.LatLng(60.186908,24.82221);
  var mapOptions = {
    zoom: 15,
    center: myLatlng
  }
  var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

  var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: 'Ankaraa koodausta'
  });
}

google.maps.event.addDomListener(window, 'load', initialize);


