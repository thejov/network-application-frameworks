$(document).ready(function() {
    $('.carousel').carousel()
});

$("#fb-post-button")[0].onclick = postToFB;

function postToFB() {
    console.log("Posting to Facebook...");
    FB.api('/me/feed', 'post', {message: 'Hello World! Ebinz!'} ,function(fbResponse){
		if(!fbResponse||fbResponse.error) {
            console.log(fbResponse.error);
        } else {
            console.log(fbResponse.id);
            $("#text-post-success").removeAttr("hidden");
        }
	});
}

$("#photo-button")[0].onclick = getPictures;

function getPictures() {
    console.log("Getting all FB pictures.");
    FB.api('/me/photos', function (response) {
      if (response && !response.error) {
        console.log(response);

        $(".carousel .item").each( function(index, item) {
            $(item).find("img").attr("src", response.data[index].source);

            if (typeof response.data[index].likes === 'undefined') {
                likes = [];
            } else {
                var likes = response.data[index].likes.data;
            }
            $(item).find(".carousel-caption h3").html("Like amount " + likes.length);

            var subCaption = $(item).find(".carousel-caption p");
            if (likes.length > 0) {
              subCaption.html(likes[0].name + " and " + (likes.length - 1) + " others like this image.");
            } else {
              subCaption.html("Nobody has liked this image.");
            }

        } );
      }
    });
}

$("#image-post-button")[0].onclick = postToFacebook;

function postToFacebook()
{
	console.log($("#image-url").val());
	//post to album
	FB.api("me/photos","post",{
		message:"facebook hullabaloo",
		url:$("#image-url").val()
	},function(fbResponse){
		if(!fbResponse||fbResponse.error) {
            console.log(fbResponse.error);
        } else {
            console.log(fbResponse.id);
            $("#pic-post-success").removeAttr("hidden");
        }
	});
}