window.fbAsyncInit = function() {
      // Here we subscribe to the auth.authResponseChange JavaScript event. This event is fired
      // for any authentication related change, such as login, logout or session refresh. This means that
      // whenever someone who was previously logged out tries to log in again, the correct case below
      // will be handled.
      FB.Event.subscribe('auth.authResponseChange', function(response) {
        // Here we specify what we do with the response anytime this event occurs.
        if (response.status === 'connected') {
          // The response object is returned with a status field that lets the app know the current
          // login status of the person. In this case, we're handling the situation where they
          // have logged in to the app.
            console.log("------- connected block");
            console.log('Welcome!  Fetching your information.... ');
            FB.api('/me', function(response) {
                console.log('Good to see you, ' + response.name + '.');
                $("#title").html('Good to see you, ' + response.name + '.');
                $("#buttons").removeAttr("disabled");
            });
        } else {
            $("#title").html('Please login to do something.');
            $("#buttons").attr('disabled', 'true');
        }

      });
};