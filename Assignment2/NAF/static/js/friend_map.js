google.maps.event.addDomListener(window, 'load', initialize);

var map;
var geocoder;

function initialize() {
  geocoder = new google.maps.Geocoder();
  var myLatlng = new google.maps.LatLng(60.186908,24.82221);
  var mapOptions = {
    zoom: 1,
    center: myLatlng
  }
  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

  var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: 'Ankaraa koodausta'
  });
}

$("#friend-map-button")[0].onclick = getFriendMap;

function getFriendMap() {
    console.log("Getting your Friend Map.");
    FB.api('/me/friends', function (response) {
      if (response && !response.error) {
        console.log(response);
        findFriendWithHometown(response);
      }
    });

}

function findFriendWithHometown(response) {
    $.each(response.data, function( index, value ) {
        getFriend(value.id);
    })
}

function getFriend(id) {
    FB.api('/' + id, function (response) {
      if (response && !response.error) {
        if (response.hometown) {
            console.log(response);
            addMarkerToMap(response.hometown.name, response.name);
        }
      }
    });
}

function addMarkerToMap(hometown, friendName) {
    // initial source: http://stackoverflow.com/questions/16982380/google-maps-javascript-api-getting-coordinates-from-address
    // small modifications done
    geocoder.geocode( { 'address': hometown}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        map.setCenter(results[0].geometry.location);
        var marker = new google.maps.Marker({
            map: map,
            position: results[0].geometry.location,
            title: friendName
        });
      } else {
        console.log("Geocode was not successful for the following reason: " + status);
      }
    });
}